#!/usr/bin/perl -w
use strict;

#Frances Nikki N. Borja
#Parallel multigenome alignment to a specific sequence


opendir TOPDIR, $ARGV[0] or die "Cannot open $ARGV[0]: $!";

open OUT, ">pi2.txt" or die "Cannot create file: $!";

while (my $directory=readdir *TOPDIR){
  next unless $directory!~/^\.+/;
  #print "$file\n";
  opendir DIR, "$ARGV[0]/$directory" or die "Cannot open $directory: $!";
  while (my $file=readdir *DIR){
    next unless $file!~/^\.+/;
    next unless $file=~/.+merge.sam/;

    my @s=split(/\./, $file);
    #print $s[0], "\n";
    my $output=$s[0];
    my $spt="$output.count.txt";   
#    print "directory: $directory\n";
#    print "file: $file\n";
#    print "$spt\n";
   
    system ("cat $ARGV[0]/$directory/$file | grep Pi2 | wc -l > $ARGV[0]/$directory/$spt");
#    print "$spt\n";
    
    open SUSPECT,"$ARGV[0]/$directory/$spt" or die "Cannot open $spt: $!";
    while (my $count=readline *SUSPECT){
      print "acc:$file\tcount=$count";
      if ($count>100){
        print OUT "$output\n";
      }
    }  

  }
#last;
}

close TOPDIR;
exit();